from django.db import models

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length = 200)
    body = models.TextField()
    pub_date = models.DateField(auto_now_add=True)
    image = models.ImageField(upload_to='media/', default = False, blank = True)

    def __str__(self):
        return self.title

    

