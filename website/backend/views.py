from django.shortcuts import render, get_object_or_404, redirect
from .models import Post
from .filters import PostFilter
from .forms import PostForm
from django.utils import timezone

def home(request):
    return render(request, 'backend/base.html')

def portfolio(request):
    return render(request, 'backend/portfolio.html')

def news(request):
    post_list = Post.objects.all()
    post_title_filter = PostFilter(request.GET, queryset = post_list)
    return render(request, 'backend/news.html', {'filter': post_title_filter, 'posts' : post_list})

def news_details(request, post_id):
    post = get_object_or_404(Post,pk=post_id)
    return render(request, 'backend/news_details.html', {'posts' : post})

def news_new_one(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.pub_date = timezone.now()
            post.save()
            return redirect('news')
    else:
        form = PostForm()

    return render(request, 'backend/news_edit.html', {'form': form})
