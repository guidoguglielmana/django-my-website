from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from backend import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home),
    path('portfolio/', views.portfolio),
    path('news/', views.news, name = "news"),
    path('news/new_one/', views.news_new_one, name='new_one'),
    url (r'^news/(?P<post_id>[0-9]+)/$', views.news_details, name = "news_details"),
    
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
